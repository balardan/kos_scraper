import kos_app
from utils import print_warning
import json

username = password = ""

try:
    data = json.load(open("./settings.json"))
    username = data["username"]
    password = data["password"]
except:
    username = input("KOS login:").strip()
    password = input("KOS password:").strip()
    print_warning("You can set these in settings.json file.\nSee settings-example.json for structure.")

filename = input("Filename where result will be saved:").strip()

kos = kos_app.KOS(username, password)
kos.get_all_data(filename)
