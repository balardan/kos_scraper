def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def obj_dict(obj):
    return obj.__dict__


class bcolors:
    WARN = "\x1b[1;33;40m"
    ERR = "\x1b[1;31;40m"
    SUCC = "\x1b[1;32;40m"
    DEF = "\x1b[0m"


def print_warning(message: str):
    print(bcolors.WARN + message + bcolors.DEF)


def print_error(message: str):
    print(bcolors.ERR + message + bcolors.DEF)


def print_success(message: str):
    print(bcolors.SUCC + message + bcolors.DEF)


