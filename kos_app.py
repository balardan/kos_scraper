import json
import utils

try:
    import mechanicalsoup
    import unidecode
except:
    utils.print_error("Dependency not found, please install it via `pip install -r requirements.txt`")
    exit(1)

urls = {}
urls['base'] = "https://kos.is.cvut.cz/kos"
urls['logout'] = urls['base'] + '/logout.do'
urls['allSubjects'] = urls['base'] + '/ttAllsubjects.do'
urls['oneSubject'] = urls['base'] + '/ttOnesubjectsCVUT.do'

semesterId = 'B172'

days = ['Po', 'Ut', 'St', 'Ct', 'Pa']


class Subject:
    def __init__(self, index, id, name):
        self.index = index
        self.id = id
        self.name = name

    def __str__(self):
        return str(self.index) + ": " + str(self.id)


class SubjectDetails:
    def __init__(self, id, time, day, type, capacity, occupied, teacher):
        self.id = id
        self.time = time
        self.day = day
        self.type = type
        self.capacity = capacity
        self.occupied = occupied
        self.teacher = teacher

    def __str__(self):
        pass


class KOS:
    def __init__(self, username=None, password=None):
        self.__username = username
        self.__password = password
        self.browser = mechanicalsoup.StatefulBrowser()
        self.page_id = None
        self.__login()
        self.__set_page_id(self.browser.get_current_page())
        self.subjects = {}

    def __login(self):
        self.browser.open(urls['logout'])
        self.browser.select_form('form[name="login"]')
        self.browser['userName'] = self.__username
        self.browser['password'] = self.__password
        self.browser.submit_selected()

    @staticmethod
    def get_page_id(data):
        return str(data).split('pageCode')[1].split('\'')[1]

    def get_url(self, url):
        return url + '?page=' + self.page_id

    def __set_page_id(self, data):
        self.page_id = self.get_page_id(data)

    @staticmethod
    def __get_time_and_day(tds):
        data = tds[5:-3]
        index = 0
        for i, e in enumerate(data):
            if e.text.strip() != "":
                index = i
                break
        day = days[int(index / 2)]
        if index % 2 != 0:
            day += "-S"
        elif data[index + 1] == "":
            day += "-L"
        hour = data[index].text.strip()
        return [day, hour]

    def __process_row(self, row):
        data = row.select('td')
        try:
            id = data[0].text
            type = data[1].text.lower()[0]
            capacity = data[3].text
            occupied = data[4].text
            teacher = unidecode.unidecode(data[2].text).strip()
            t2 = self.__get_time_and_day(data)
            day = t2[0]
            time = t2[1]
            res = SubjectDetails(id, time, day, type, capacity, occupied, teacher)
            return res
        except:
            return None

    def get_list_of_subjects(self):
        self.browser.open(self.get_url(urls['allSubjects']))
        data = self.browser.get_current_page().find_all('tr', class_="tableRow1")
        data += self.browser.get_current_page().find_all('tr', class_="tableRow2")
        res = []
        for i, e in enumerate(data):
            id = e.select_one('td:nth-of-type(1)').text
            name = e.select_one('td:nth-of-type(2)').text
            res.append(Subject(i, id, name))
        return res

    def get_subject_details(self, subject: Subject, save=None):
        try:
            self.browser.open(self.get_url(urls['allSubjects']))
            self.browser.select_form('form[name="ttOnesubjectsCVUT"]')
            self.browser['subjectId'] = subject.id
            self.browser['subjectName'] = subject.index
            self.browser['action'] = 'subjectView'
            self.browser['semesterId'] = semesterId
            self.browser.get_current_form().form['action'] += '?page=' + self.page_id
            self.browser.submit_selected()
            t = self.browser.get_current_page().select_one('.ttSubjectRow1').parent.parent
            rows = t.select('tr')[1:]
            res = []
            for i, e in enumerate(rows):
                r = self.__process_row(e)
                if not r is None:
                    res.append(r)
            if save:
                self.subjects[subject.id] = res
            return res
        except:
            return None

    def get_all_data(self, file=False):
        print("Getting subject list.")
        subjects = self.get_list_of_subjects()
        print("Found " + str(len(subjects)) + " subjects.")
        utils.print_progress_bar(0, len(subjects), prefix='Downloading:', suffix='Complete', length=50)
        errors = []
        for i, e in enumerate(subjects):
            res = True
            if not e.id in self.subjects:
                res = self.get_subject_details(e, True)
            if res is None:
                errors.append(str(e.id) + " failed")
            utils.print_progress_bar(i+1, len(subjects), prefix='Downloading:', suffix='Complete', length=50)
        for e in errors:
            utils.print_error(e)
        if not file is False:
            f = open(file, "w")
            d = json.dumps(self.subjects, default=utils.obj_dict)
            f.write(d)
        utils.print_success("Done!")
        return self.subjects
